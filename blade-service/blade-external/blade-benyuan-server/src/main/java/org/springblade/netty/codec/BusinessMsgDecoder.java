package org.springblade.netty.codec;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageDecoder;
import io.undertow.util.CopyOnWriteMap;
import org.apache.commons.lang.ArrayUtils;
import org.springblade.common.constant.NumberConstant;
import org.springblade.netty.business.handle.system.SimpleLimitedTraffic;
import org.springblade.netty.protocol.MsgProtocol;
import org.springblade.netty.protocol.request.ServerRequest;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 自定义业务解码器
 * @author lijiamin
 */
public class BusinessMsgDecoder extends ByteToMessageDecoder {

	/** 消息存储 */
	private static ConcurrentHashMap<String, byte[]> reqMsgMap = new ConcurrentHashMap<>();

	public static void delMsgMap(String address) {
		reqMsgMap.remove(address);
	}

	public static void createMsgMap(String address) {
		reqMsgMap.put(address, new byte[]{});
	}

	private byte[] getOnDelArrayMsg(ChannelHandlerContext channelHandlerContext, byte[] req) {
		String remoteAddress = channelHandlerContext.channel().remoteAddress().toString();
		byte[] reqMsgAll = ArrayUtils.addAll(reqMsgMap.get(remoteAddress), req);
		reqMsgMap.put(remoteAddress, new byte[]{});
		return reqMsgAll;
	}

	private void copyOnArrayMsg(ChannelHandlerContext channelHandlerContext, byte[] req) {
		String remoteAddress = channelHandlerContext.channel().remoteAddress().toString();
		byte[] reqMsgAll = ArrayUtils.addAll(reqMsgMap.get(remoteAddress), req);
		reqMsgMap.put(remoteAddress, reqMsgAll);
	}

	/**
	 * 消息解码
	 * flag(1 byte)+length(NumberConstant.FOUR byte,后边内容的长度)+protocol code(NumberConstant.FOUR byte)+content
	 * length的长度包括  ：消息号 + 内容
	 * @param channelHandlerContext
	 * @param byteBuf
	 * @param list
	 * @throws Exception
	 */
	@Override
	protected void decode(ChannelHandlerContext channelHandlerContext, ByteBuf byteBuf, List<Object> list) {
		try {
			// 简易限流器 应该做异步队列 暂时先这样
			SimpleLimitedTraffic.addTrafficOnBucket(channelHandlerContext.channel().remoteAddress().toString());

			Integer dateLength = MsgProtocol.flagSize + MsgProtocol.lengthSize + MsgProtocol.msgCodeSize;
			// 数据包长度
			int readableBytes = byteBuf.readableBytes();

			// if (byteBuf.readableBytes() < dateLength) {
			// 	// decode -- 数据包长度不足
			// 	return;
			// }

			// 返回表示 ByteBuf 当前可读取的字节数
			byte[] req = new byte[readableBytes];
			// 把 ByteBuf 里面的数据全部读取到字节数组中
			byteBuf.readBytes(req);
			byte[] reqMsgAll = getOnDelArrayMsg(channelHandlerContext, req);
			// 循环粘包处理
			List<byte[]> resultByes = packageHandle(channelHandlerContext, reqMsgAll);
			for (byte[] resultBye : resultByes) {

				byte codeByte3 = resultBye[NumberConstant.SIX];
				int codeTenSix10000 = conversionNumberSix(codeByte3) * 10000;

				byte codeByte2 = resultBye[NumberConstant.SEVEN];
				int codeTenSix100 = conversionNumberSix(codeByte2) * 100;

				byte codeByte = resultBye[NumberConstant.EIGHT];
				int codeTenSix10 = conversionNumberSix(codeByte);

				// code result
				int codeTenSix = codeTenSix10000 + codeTenSix100 + codeTenSix10;

				// System.out.println("消息请求协议码 = " + codeTenSix + " ------ 消息长度 = " + readableBytes);
				if (readableBytes == NumberConstant.NINE) {
					// 无参传递
					list.add(new ServerRequest(codeTenSix, ""));
				} else {
					// 有参传递
					byte[] detailBytes = Arrays.copyOfRange(resultBye, 11, resultBye.length);
					String paramStr = new String(detailBytes);
					list.add(new ServerRequest(codeTenSix, paramStr));
				}
			}
		} catch (Exception e) {
			// 防止因为请求码异常导致端口无了 ...test
			e.printStackTrace();
		}
	}

	/**
	 * 递归处理循环粘包
	 * @param req 消息
	 * @return
	 */
	private List<byte[]> packageHandle(ChannelHandlerContext channelHandlerContext, byte[] req) {
		List<byte[]> bytes = new ArrayList<>();
		return stickCut(channelHandlerContext, bytes, req);
	}

	/**
	 * 切包
	 * @param req
	 * @return
	 */
	private List<byte[]> stickCut(ChannelHandlerContext channelHandlerContext, List<byte[]> bytes, byte[] req) {
		// 数据包完整性校对
		Integer dateLength = MsgProtocol.flagSize + MsgProtocol.lengthSize + MsgProtocol.msgCodeSize;
		if (req.length != 0 && req != null) {
			// 参数体长度
			Integer codeLength = Integer.valueOf(req[NumberConstant.FOUR]);
			if (codeLength < 0) {
				codeLength = Integer.valueOf(codeLength) + 256;
			}
			byte[] ofRange = Arrays.copyOfRange(req, 0, codeLength + NumberConstant.FOUR + NumberConstant.ONE);
			if (req.length < (codeLength + 5)) {
				copyOnArrayMsg(channelHandlerContext, req);
				return bytes;
			}
			if (req.length < dateLength) {
				// decode -- 数据包长度不足
			}
			if (ofRange[0] != 1) {
				// flag 错误
				copyOnArrayMsg(channelHandlerContext, req);
				return bytes;
			} else {
				bytes.add(ofRange);
			}
			if (req.length != ofRange.length) {
				// 粘包了 递归处理
				byte[] copyByte = Arrays.copyOfRange(req, ofRange.length, req.length);
				return stickCut(channelHandlerContext, bytes, copyByte);
			}
		}
		return bytes;
	}

	/**
	 * 字节进制转换
	 * @param codeByte
	 * @return
	 */
	private int conversionNumberSix(byte codeByte) {
		String toHexCodeStr = Integer.toHexString(codeByte);
		if (toHexCodeStr.contains("f")) {
			toHexCodeStr = toHexCodeStr.substring(toHexCodeStr.length() - 2, toHexCodeStr.length());
		}
		return new Integer(toHexCodeStr);
	}


}
