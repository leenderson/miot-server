package org.springblade.netty.business.handle.system;

import com.alibaba.fastjson.JSONObject;
import io.netty.channel.ChannelHandlerContext;
import org.springblade.common.config.MqQueueConfig;
import org.springblade.core.tool.utils.SpringUtil;
import org.springblade.netty.protocol.request.ServerRequest;
import org.springframework.amqp.rabbit.core.RabbitTemplate;

import java.net.SocketAddress;
import java.util.HashMap;
import java.util.Map;

/**
 * 审计日志处理器
 * @author 李家民
 */
public class AuditLogHandler {

	private static RabbitTemplate rabbitTemplate;

	private static RabbitTemplate getRabbitClient() {
		if (rabbitTemplate == null) {
			rabbitTemplate = SpringUtil.getBean(RabbitTemplate.class);
		}
		return rabbitTemplate;
	}

	/**
	 * 插入审计日志
	 * @param channelHandlerContext
	 * @param serverRequest
	 */
	public static void pushAuditLog(ChannelHandlerContext channelHandlerContext, ServerRequest serverRequest) {
		// TODO
		Map<String, Object> auditMap = new HashMap<>();
		auditMap.put("path", serverRequest.getCode());
		auditMap.put("accessMode", "socket");
		auditMap.put("realName", channelHandlerContext.channel().remoteAddress().toString());
		auditMap.put("userId", 123456);
		auditMap.put("param", serverRequest.getData());
		String toJSONFromAuditLog = JSONObject.toJSONString(auditMap);
		getRabbitClient().convertAndSend(MqQueueConfig.LOG_AUDIT_EXCHANGE, MqQueueConfig.LOG_AUDIT_QUEUE, toJSONFromAuditLog);
	}


}
