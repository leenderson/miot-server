package org.springblade.mqtt;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.springframework.stereotype.Component;

/**
 * MQTT处理器
 * @author 李家民
 */
@Component
public class MqttHandle implements MqttCallback {

	/**
	 * 连接丢失
	 * @param cause
	 */
	@Override
	public void connectionLost(Throwable cause) {
		System.out.println("connection lost：" + cause.getMessage());
		MqttInitialized.reconnection();
	}

	/**
	 * 收到消息
	 * @param topic
	 * @param message
	 */
	@Override
	public void messageArrived(String topic, MqttMessage message) {
		System.out.println("Received message: \n  topic：" + topic
			+ "\n  Qos：" + message.getQos()
			+ "\n  payload：" + new String(message.getPayload()));
	}

	/**
	 * 消息传递成功
	 * @param token
	 */
	@Override
	public void deliveryComplete(IMqttDeliveryToken token) {
		System.out.println("deliveryComplete");
	}

}
