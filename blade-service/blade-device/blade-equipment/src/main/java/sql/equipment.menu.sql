INSERT INTO `blade_menu`(`id`, `parent_id`, `code`, `name`, `alias`, `path`, `source`, `sort`, `category`, `action`, `is_open`, `remark`, `is_deleted`)
VALUES ('1563372425893228546', '1563372267092643841', 'equipment', '', 'menu', '/equipment/equipment', NULL, 1, 1, 0, 1, NULL, 0);
INSERT INTO `blade_menu`(`id`, `parent_id`, `code`, `name`, `alias`, `path`, `source`, `sort`, `category`, `action`, `is_open`, `remark`, `is_deleted`)
VALUES ('1563372425893228547', '1563372425893228546', 'equipment_add', '新增', 'add', '/equipment/equipment/add', 'plus', 1, 2, 1, 1, NULL, 0);
INSERT INTO `blade_menu`(`id`, `parent_id`, `code`, `name`, `alias`, `path`, `source`, `sort`, `category`, `action`, `is_open`, `remark`, `is_deleted`)
VALUES ('1563372425893228548', '1563372425893228546', 'equipment_edit', '修改', 'edit', '/equipment/equipment/edit', 'form', 2, 2, 2, 1, NULL, 0);
INSERT INTO `blade_menu`(`id`, `parent_id`, `code`, `name`, `alias`, `path`, `source`, `sort`, `category`, `action`, `is_open`, `remark`, `is_deleted`)
VALUES ('1563372425893228549', '1563372425893228546', 'equipment_delete', '删除', 'delete', '/api/blade-equipment/equipment/remove', 'delete', 3, 2, 3, 1, NULL, 0);
INSERT INTO `blade_menu`(`id`, `parent_id`, `code`, `name`, `alias`, `path`, `source`, `sort`, `category`, `action`, `is_open`, `remark`, `is_deleted`)
VALUES ('1563372425893228550', '1563372425893228546', 'equipment_view', '查看', 'view', '/equipment/equipment/view', 'file-text', 4, 2, 2, 1, NULL, 0);
