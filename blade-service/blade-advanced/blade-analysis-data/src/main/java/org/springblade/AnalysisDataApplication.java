package org.springblade;

import org.springblade.core.cloud.client.BladeCloudApplication;
import org.springblade.core.launch.BladeApplication;

/**
 * 数据分析模块
 * @author 李家民
 */
@BladeCloudApplication
public class AnalysisDataApplication {
	public static void main(String[] args) {
		BladeApplication.run("blade-analysis-data", AnalysisDataApplication.class, args);
	}
}
