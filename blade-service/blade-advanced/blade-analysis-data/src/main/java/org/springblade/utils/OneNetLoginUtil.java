package org.springblade.utils;

import java.util.Map;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * 移动OneNet平台登录
 * @author 李家民
 */
public class OneNetLoginUtil {

	private static final String aiKey = "2c4dd596ebb84354bb819c7850be8111";
	private static final String secretKey = "bccf1d7bdb994bdc91f1ea468f6b8ed1";

	private static Gson gson = new GsonBuilder().setPrettyPrinting().create();

	public static String accessToken() {
		//服务地址
		String path = "http://ai.heclouds.com:9090/v1/user/app/accessToken?aiKey=" + aiKey + "&secretKey=" + secretKey;
		try {
			//调用
			String result = HttpUtil.get(path, null);
			Map map = gson.fromJson(result, Map.class);
			Object data = map.get("data");
			return gson.fromJson(gson.toJson(data), Map.class).get("accessToken").toString();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "";
	}

}
