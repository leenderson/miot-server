[TOC]

# Zipkin

## 一、入门

[OpenZipkin · A distributed tracing system](https://zipkin.io/)

​	Zipkin 是 Twitter 的一个开源项目，它基于 Google Dapper 实现，它致力于收集服务的定时数据，以解决微服务架构中的延迟问题，包括数据的收集、存储、查找和展现。 我们可以使用它来收集各个服务器上请求链路的跟踪数据，并通过它提供的 REST API 接口来辅助我们查询跟踪数据以实现对分布式系统的监控程序，从而及时地发现系统中出现的延迟升高问题并找出系统性能瓶颈的根源。除了面向开发的 API 接口之外，它也提供了方便的 UI 组件来帮助我们直观的搜索跟踪信息和分析请求链路明细，比如：可以查询某段时间内各用户请求的处理时间等

<img src="https://csdn-pic-1301850093.cos.ap-guangzhou.myqcloud.com/csdn-pic/Zipkin-逻辑框架图-1.png" style="zoom:80%;" />

### Docker部署

```shell
docker pull openzipkin/zipkin
docker run -itd -p 9411:9411 openzipkin/zipkin
```

### 与SpringBoot集成

导入依赖

```xml
<dependency>
            <groupId>org.springframework.cloud</groupId>
            <artifactId>spring-cloud-starter-zipkin</artifactId>
        </dependency>
```

配置yaml文件

```yaml
spring:
  zipkin:
    base-url: http://47.106.207.254:9411
```

首先进行接口访问后，再打开zipkinUI进行查看

![](https://csdn-pic-1301850093.cos.ap-guangzhou.myqcloud.com/csdn-pic/zipkin-基础UI界面-1.png)

1







[Zipkin — 微服务链路跟踪. - JMCui - 博客园 (cnblogs.com)](https://www.cnblogs.com/jmcui/p/10940372.html)



















# over