package org.springblade.core.log.mq;

import com.alibaba.fastjson.JSONObject;
import org.springblade.common.config.MqQueueConfig;
import org.springblade.core.log.entity.LogAudit;
import org.springblade.core.log.service.ILogAuditService;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * MQ消息监听
 * @author 李家民
 */
@Component
public class MessageListener {

	@Autowired
	private ILogAuditService logAuditService;

	/**
	 * 用户操作行为审计信息
	 * @param message JSON DATA
	 */
	@RabbitListener(queues = MqQueueConfig.LOG_AUDIT_QUEUE)
	public void logAuditQueue(Message message) {
		LogAudit logAudit = JSONObject.parseObject(new String(message.getBody()), LogAudit.class);
		logAuditService.save(logAudit);
	}

}
