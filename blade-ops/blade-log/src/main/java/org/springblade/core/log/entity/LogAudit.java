/**
 * Copyright (c) 2018-2028, Chill Zhuang 庄骞 (smallchill@163.com).
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.springblade.core.log.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import org.springblade.core.mp.base.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 实体类
 *
 * @author Blade
 * @since 2022-08-28
 */
@Data
@TableName("blade_log_audit")
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "LogAudit对象", description = "LogAudit对象")
public class LogAudit extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @ApiModelProperty(value = "主键")
    private Long id;
    /**
     * 访问方式
     */
    @ApiModelProperty(value = "访问方式")
    private String accessMode;
    /**
     * 请求人真实名称
     */
    @ApiModelProperty(value = "请求人真实名称")
    private String realName;
    /**
     * 用户id
     */
    @ApiModelProperty(value = "用户id")
    private Long userId;
    /**
     * 访问路径
     */
    @ApiModelProperty(value = "访问路径")
    private String path;
    /**
     * 携带参数
     */
    @ApiModelProperty(value = "携带参数")
    private String param;


}
