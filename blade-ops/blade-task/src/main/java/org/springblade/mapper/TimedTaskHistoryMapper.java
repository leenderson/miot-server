package org.springblade.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springblade.entity.TimedTaskHistory;

public interface TimedTaskHistoryMapper  extends BaseMapper<TimedTaskHistory> {
}
