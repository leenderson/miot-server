package org.springblade.controller;

import cn.hutool.core.util.ReflectUtil;
import org.springblade.business.SimpleBusiness;
import org.springblade.core.tool.api.R;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.lang.reflect.Method;

/**
 * 测试用控制器
 * @author 李家民
 */
@RestController
@RequestMapping("/test")
public class TestController {

	/**
	 * 简易定时任务执行器
	 * @param name
	 * @return
	 */
	@GetMapping("/executionMethod")
	public R executionMethod(String name) {
		SimpleBusiness simpleBusiness = new SimpleBusiness();
		Object resData = ReflectUtil.invoke(simpleBusiness, name, null);
		return R.data(resData);

		// 获取类中的方法
		// Method[] methods = ReflectUtil.getMethods(SimpleBusiness.class);
	}

}
